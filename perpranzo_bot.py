import os
import time
import re
import sys
from slackclient import SlackClient

## adapted from
## https://www.fullstackpython.com/blog/build-first-slack-bot-python.html

# instantiate Slack client
slack_client = SlackClient(os.environ.get('SLACK_BOT_TOKEN'))
# bot's user ID in Slack: value is assigned after the bot starts up
bot_id = None

# constants
RTM_READ_DELAY = 30 # 30 second delay between reading from RTM
EXAMPLE_COMMAND = "menu"
MENTION_REGEX = "^<@(|[WU].+?)>(.*)"

CHANNEL_MENSA = "GAX14AKT2"


def parse_bot_commands(slack_events):
    """
        Parses a list of events coming from the Slack RTM API to find bot commands.
        If a bot command is found, this function returns a tuple of command and channel.
        If its not found, then this function returns None, None.
    """
    for event in slack_events:
        if event["type"] == "message" and not "subtype" in event:
            user_id, message = parse_direct_mention(event["text"])
            if user_id == bot_id:
                return message, event["channel"]
    return None, None

def parse_direct_mention(message_text):
    """
        Finds a direct mention (a mention that is at the beginning) in message text
        and returns the user ID which was mentioned. If there is no direct mention, returns None
    """
    matches = re.search(MENTION_REGEX, message_text)
    # the first group contains the username, the second group contains the remaining message
    return (matches.group(1), matches.group(2).strip()) if matches else (None, None)

def handle_command(command, channel):
    """
        Executes bot command if the command is known
    """
    # Default response is help text for the user
    default_response = "Not sure what you mean. Try *{}*.".format(EXAMPLE_COMMAND)

    response = None
    if command == "menu":
        response = menu()

    # Sends the response back to the channel
    slack_client.api_call(
        "chat.postMessage",
        channel=channel,
        text=response or default_response
    )

def menu():
    import tillunch.tillunch as tillunch
    import tillunch.days as days
    menus = tillunch.tillunch(
        tillunch.all_restaurants,
        days.today(),
        u"slack",
        False,
        False,
        False
    )
    menus = u"\n\n".join(menus)
    return "Today's menu:\n\n" + menus

if __name__ == "__main__":
    if slack_client.rtm_connect(with_team_state = False):
        print "Bot PerPranzo connected and running!"
        # Read bot's user ID by calling Web API method `auth.test`
        bot_id = slack_client.api_call("auth.test")["user_id"]
        while True:
            command, channel = parse_bot_commands(slack_client.rtm_read())
            if command:
                handle_command(command, channel)
            time.sleep(RTM_READ_DELAY)
        print "Bot PerPranzo terminated."
    else:
        print "Connection failed. Exception traceback printed above."
