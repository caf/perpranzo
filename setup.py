from setuptools import setup, find_packages

setup(
    name='perpranzo',
    version='0.1a1',
    description='Tool to fetch lunch menus',
    url='https://bitbucket.org/caf/perpranzo',
    author='Carlo A. Furia',
    author_email='',
    license='GPLv3+',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: End Users/Desktop',
        'Topic :: Utilities',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
    ],
    packages=["tillunch"],
    install_requires=['selenium', 'beautifulsoup4'],
    package_data={
    },
    data_files=[],
    entry_points={
        'console_scripts': [
            'perpranzo=tillunch.tillunch:main',
        ],
    },
)
