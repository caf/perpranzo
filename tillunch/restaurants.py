# -*- coding: utf-8 -*-

from menu import PrintableMenu
from days import today, day_name


class Mensa(PrintableMenu):

    def __init__(self, on_day):
        super(Mensa, self).__init__(on_day)
        self.restaurant = u"Mensa"
        self.sections = [(u"Piatto del giorno",),
                         (u"Pasta del giorno",),
                         (u"Vegetariano", u"Piatto vegetariano")]

    def urls(self):
        return [u"https://www.desk.usi.ch/it/mensa-e-menu-settimanale-campus-di-lugano"]

    def find_menu(self, soup):
        for day in soup.find_all("strong"):
            if day.text.startswith(day_name(self.on_day, "italian").capitalize()):
                menu_content = day.parent.parent
                for section_list in self.sections:
                    for section in section_list:
                        section_content = menu_content.find("strong", text = section)
                        if section_content:
                            self.menu += [section_content.parent.text.strip().splitlines()[1:]]
                menu_lines = menu_content.text.strip().splitlines()
                self.menu_day = menu_lines[0]
                return True
        return False


class Liceo1(PrintableMenu):

    def __init__(self, on_day):
        super(Liceo1, self).__init__(on_day)
        self.restaurant = u"Liceo cantonale Lugano 1"
        self.sections = [(u"Menu completo", u"menuCompleto",),
                         (u"Piatto del giorno", u"resto_piattoGiorno",),
                         (u"Menu vegetariano", "resto_menuVegetariano",),
                         (u"Pasta del giorno", u"resto_pastaGiorno",),
                         (u"Piatto freddo", u"resto_piattoFreddo",)]

    def urls(self):
        import datetime
        self.url_template = "https://m4.ti.ch/index.php?id=33936&user_decsresto_pi1[post]=23&user_decsresto_pi1[giorno]="
        ## absolute date from relative date
        on_day_date = datetime.datetime.today() + datetime.timedelta(self.on_day - today())
        return [self.url_template + on_day_date.strftime("%d.%m.%Y")]

    def find_menu(self, soup):
        for day in soup.find_all("h3", attrs = {"class": "resto_data"}):
            if day.text.startswith(day_name(self.on_day, "italian").capitalize()):
                menu_content = day.parent.parent
                for section_list in self.sections:
                    for section in section_list:
                        section_content = menu_content.find("div", attrs = {"id": section})
                        if section_content:
                            menu = ""
                            for item in section_content.find_all("p"):
                                try:
                                    cls = item.attrs["class"][0]
                                    if cls.startswith(u"menuTitolo") \
                                       or cls.startswith(u"prezzo") \
                                       or cls.startswith(u"resto_prezzo"):
                                        pass
                                    elif cls.startswith(u"menuTipoPiatto"):
                                        menu += item.text.strip().capitalize() + ": "
                                    else:
                                        menu += ", ".join(map(lambda s: s.strip(), item.text.splitlines())) + "\n"
                                except:
                                    raise
                            self.menu += [menu.splitlines()]
                menu_lines = menu_content.text.strip().splitlines()
                self.menu_day = menu_lines[0]
                return True
        return False


class Bistro(PrintableMenu):

    def __init__(self, on_day):
        super(Bistro, self).__init__(on_day)
        self.restaurant = u"Charlie's Bistro"
        self.base_url = "https://www.facebook.com/timeoutbycharlie/posts/"

    ### Gets the URL of individual posts by parsing the Facebook main board
    def urls(self):
        import subprocess, tempfile, re, os
        from bs4 import BeautifulSoup        
        outfile = next(tempfile._get_candidate_names())
        scraped = True
        try:
            subprocess.check_call(["cutycapt",
                                   "--url=" + self.base_url,
                                   "--out=" + outfile,
                                   "--out-format=html"])
        except CalledProcessError:
            scraped = False
        base = self.base_url[[i for i, letter in enumerate(self.base_url) \
                              if letter == "/"][-3]:]
        if scraped:
            pids = []
            with open(outfile, "r") as fp:
                soup = BeautifulSoup(fp, "html.parser")
                for day in soup.find_all("a", attrs = {"class": "see_more_link"}):
                    if "href" in day.attrs:
                        match = re.search("^" + base + "([0-9]+)$",
                                          day.attrs["href"])
                        if match is not None:
                            pids += [match.group(1)]
        else:
            pids = []
        try:
            os.remove(outfile)
        except OSError:
            pass
        on_today = today()
        if self.on_day <= on_today:
            offset = on_today - self.on_day  # today == first match == latest post
        else:
            offset = on_today + (6 - self.on_day + 1)  # on_day last week
        while True:
            try:
                yield self.base_url + pids[offset]
                offset += 1  # if it didn't work, move on
            except:
                return

    def souped(self):
        import urllib2, subprocess, tempfile, os
        from bs4 import BeautifulSoup
        for url in self.urls():
            if url is None:
                break
            outfile = next(tempfile._get_candidate_names())
            scraped = True
            try:
                subprocess.check_call(["cutycapt",
                                       "--url=" + url,
                                       "--out=" + outfile,
                                       "--out-format=html"])
            except CalledProcessError:
                scraped = False
            if scraped:
                with open(outfile, 'r') as fp:
                    soup = BeautifulSoup(fp, "html.parser")
            try:
                os.remove(outfile)
            except OSError:
                pass
            if scraped:
                yield soup
            else:
                return

    def find_menu(self, soup):
        for day in soup.find_all("p"):
            if day.text.startswith(day_name(self.on_day, "swedish").title()):
                menu = day.parent.get_text(separator = "\n").splitlines()
                self.menu_day = menu[0]
                self.menu = menu[1:]
                return True
        return False


class Fingys(Bistro):

    def __init__(self, on_day):
        super(Fingys, self).__init__(on_day)
        self.restaurant = u"Fingy's"
        self.base_url = "https://www.facebook.com/fingys.ch/posts/"

    def find_menu(self, soup):
        import re
        name_today = day_name(self.on_day, "italian")
        re_todaysmenu = re.compile(".*#" + name_today.lower()[:-1])
        for element in soup.find_all("p"):
            if re_todaysmenu.match(element.text, re.IGNORECASE):
                menu = [s.strip() \
                        for s in element.parent.get_text(separator = "\n").splitlines()]
                ## remove boilerplate
                menu = [s for s in menu if (
                    s
                    and not s.startswith(u"#")
                    and not s.startswith(u"dailymenu")
                    and not re.match(".*" + name_today[:-1], s, re.IGNORECASE)
                )]
                ## split by item -
                new_menu = []
                for m in menu:
                    if m.startswith('-'):
                        n = m.strip('-').strip()
                        new_menu += [n]
                    else:
                        new_menu[-1] += ' ' + m
                self.menu = new_menu
                self.menu_day = name_today.title()
                return True
        return False
    

class Wijkanders(PrintableMenu):

    def __init__(self, on_day):
        super(Wijkanders, self).__init__(on_day)
        self.restaurant = u"Wijkanders"

    def urls(self):
        return [u"http://www.wijkanders.se/restaurangen/"]

    def find_menu(self, soup):
        for day in soup.find_all("strong"):
            if day.text.startswith(day_name(self.on_day, "swedish").upper()):
                menu_lines = day.parent.text.rstrip().lstrip().splitlines()
                self.menu_day = menu_lines[0]
                self.menu = menu_lines[1:]
                return True
        return False

class Einstein(PrintableMenu):

    def __init__(self, on_day):
        super(Einstein, self).__init__(on_day)
        self.restaurant = u"Einstein"

    def urls(self):
        return [u"http://www.butlercatering.se/einstein"]

    def find_menu(self, soup):
        for day in soup.find_all("h3", attrs = {"class": "field-label"}):
            if day.text.startswith(day_name(self.on_day, "swedish").title()):
                menu_lines = day.parent.text.rstrip().rstrip().splitlines()
                self.menu_day = menu_lines[1]
                self.menu = menu_lines[2:]
                return True
        return False


class OOTO(PrintableMenu):

    def __init__(self, on_day):
        super(OOTO, self).__init__(on_day)
        self.restaurant = u"OOTO"

    def urls(self):
        return [u"http://ooto.se/sv/lunchmeny/"]

    def find_menu(self, soup):
        on_today = day_name(self.on_day, "swedish").upper()
        for day in soup.find_all("h2",
                                 attrs = {"class": "av-special-heading-tag",
                                          "itemprop": "headline"}):
            if day.text.startswith(on_today):
                block_menu, idx = [], -1
                begin_at = 0
                for el in day.parent.parent.parent.children:
                    try:
                        block_menu += [el.text]
                        idx += 1
                        if el.text.startswith(on_today):
                            begin_at = idx
                    except AttributeError:
                        pass
                self.menu_day = block_menu[begin_at]
                self.menu = block_menu[begin_at+1:begin_at+4]
                return True
        return False


class MrP(PrintableMenu):

    def __init__(self, on_day):
        super(MrP, self).__init__(on_day)
        self.restaurant = u"Mr.P"

    def urls(self):
        return [u"http://www.mr-p.se/lunch/"]

    def find_menu(self, soup):
        for day in soup.find_all("h3"):
            if day.text.startswith(day_name(self.on_day, "swedish").title()):
                menu = [day]
                for i in range(5):
                    menu += [menu[-1].nextSibling]
                for veg in soup.find_all("h3"):
                    if veg.text.startswith("Veckans vegetariska"):
                        menu += [veg,
                                 veg.nextSibling,
                                 veg.nextSibling.nextSibling]
                self.menu_day = menu[0].text
                self.menu = [e.text for e in menu[1:] if e != u"\n"]
                return True
        return False


class Thai(PrintableMenu):

    def __init__(self, on_day):
        super(Thai, self).__init__(on_day)
        self.restaurant = u"Thai (Arojj Dii)"

    def urls(self):
        return [u"http://arojjdii2.kvartersmenyn.se/"]

    def find_menu(self, soup):
        self.menu_day = day_name(self.on_day, "english").title()
        menu = soup.find("div", attrs = {"class": "meny"})
        self.menu = menu.get_text(separator = "\n").splitlines()
        return True


class Linsen(PrintableMenu):

    def __init__(self, on_day):
        super(Linsen, self).__init__(on_day)
        self.restaurant = u"Linsen Café"
    
    def urls(self):
        return[u"http://www.cafelinsen.se/menyer/cafe-linsen-lunch-meny.pdf"]

    def souped(self):
        import urllib2
        import subprocess, tempfile, re, os
        urls = self.urls()
        for url in urls:
            self.url = url
            pdf_file = next(tempfile._get_candidate_names())
            with open(pdf_file, 'wb') as f:
                f.write(urllib2.urlopen(url).read())
            try:
                menu = subprocess.check_output(["pdftotext",
                                                pdf_file, "-"]).decode("utf-8")
            except CalledProcessError:
                menu = None
            try:
                os.remove(pdf_file)
            except OSError:
                pass
            menu_lines = [el.strip() for el in menu.splitlines()]
            yield menu_lines

    def find_menu(self, soup):
        try:
            today_string = day_name(self.on_day, "swedish").title()
            idx_today = [i for i in range(len(soup)) if today_string[:3] == soup[i][:3]][0]
            idx_weekly = [i for i in range(len(soup)) \
                          if soup[i].startswith(u"Stående rätter")][0]
            self.menu_day = soup[idx_today]
            self.menu = soup[idx_today+1:idx_today+4] + soup[idx_weekly:-1]
            return True
        except Exception as e:
            raise e
        return False


class IndianBBQ(PrintableMenu):

    def __init__(self, on_day):
        super(IndianBBQ, self).__init__(on_day)
        self.restaurant = u"Indian BBQ (Landala)"
    
    def urls(self):
        return[u"http://www.indianbarbeque.se/lunch-1"]

    def souped(self):
        import urllib2
        from bs4 import BeautifulSoup        
        from selenium import webdriver
        from selenium.webdriver.support.ui import WebDriverWait
        from selenium.webdriver.support import expected_conditions as EC
        from selenium.webdriver.common.by import By
        from selenium.common.exceptions import TimeoutException
        import time
        for url in self.urls():
            browser = webdriver.Firefox() # launch an instance of Firefox
            browser.get(url) # navigate to url
            time.sleep(3)
            delay = 3
            for i in range(3):  # 3 attempts at getting the page
                delay += 2
                try:  # wait until id SITE_CONTAINER has been loaded into the page
                    page = (
                        WebDriverWait(browser, delay)
                        .until(EC.presence_of_element_located((By.ID,
                                                               'SITE_CONTAINER')))
                    )
                except TimeoutException:
                    page = None
                if page is not None:
                    # get rendered HTML            
                    innerHTML = browser.execute_script("return document.body.innerHTML")
                    if len(innerHTML) > 0:
                        break
            browser.close() # close browser instance
            soup = BeautifulSoup(innerHTML, "html.parser")
            yield soup

    def find_menu(self, soup):
        for day in soup.find_all("span"):
            if day.text.startswith(day_name(self.on_day, "swedish").title()):
                menu = [day.parent]
                for i in range(2*2*3):
                    try:
                        menu += [menu[-1].nextSibling]
                    except AttributeError:
                        break
                menu = [e.text for e in menu if e is not None and e != u"\n"]
                self.menu_day = menu[0]
                self.menu = menu[1:]
                return True
        return False


