#! /usr/bin/env python2
# -*- coding: utf-8 -*-

# Copyright 2017 Carlo A. Furia

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

all_restaurants = [u"Mensa", u"Liceo1"]
ALL = u"ALL"

out_layouts = [u"full", u"compact"]


from restaurants import *


def tillunch(restaurants, day, layout, url, debug, printout = False):
    import sys
    this_module = sys.modules[__name__]
    menus = []
    for r in restaurants:
        try:
            cls = getattr(this_module, r)
            menu = cls(day)
            menu.set_layout(layout)
            menu.include_url = url
            menu.get_menu()
            menus += [menu.text()]
        except Exception as e:
            menus += [r + u": something went wrong fetching"]
            if debug:
                print >>sys.stderr, str(e)
    if printout:
        print u"\n\n".join(menus)
    else:
        return menus

def main():
    import argparse, days
    parser = argparse.ArgumentParser(
        description="Fetches the daily menus in a few restaurants around USI Lugano.")
    parser.add_argument("-d", "--day", dest="day", default=days.today(), type=int, choices=range(7),
                        help="day of week to fetch the menus for (default: %(default)s), " + \
                             "numbered from monday")
    parser.add_argument("-u", "--url", dest="url", action="store_true",
                        help="add urls of fetched pages")
    parser.add_argument("-v", "--invert", dest="inv", action="store_true",
                        help="the listed restaurants are to be excluded from the search")
    parser.add_argument("--layout", choices=out_layouts, default="full",
                        help="output formatting (default: %(default)s)")
    parser.add_argument("--debug", dest="debug", action="store_true",
                        help="print debug information if something goes wrong")
    parser.add_argument("rests", nargs="*", default = ALL, 
                        choices = [ALL] + all_restaurants,
                        help="restaurants to be included in the search (default: %s)" % ALL)
    args = parser.parse_args()
    if type(args.rests) is list:
        restaurants = args.rests
    elif args.rests == ALL:
        restaurants = all_restaurants
    if args.inv:
        restaurants = [r for r in all_restaurants if r not in restaurants]
    tillunch(restaurants, args.day, args.layout, args.url, args.debug, True)


if __name__ == "__main__":
    main()
