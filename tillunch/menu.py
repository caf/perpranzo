# -*- coding: utf-8 -*-


class Menu(object):
    
    def __init__(self, on_day):
        self.on_day = on_day
        self.menu_day = u""
        self.menu = []
        self.restaurant = u""
        self.sections = [(u"",)]
        self.url = None

    def url(self):
        """
        Return a list of guessed URLs including self's menu on day
        self.on_day, ordered from the most likely.
        """
        return []

    def souped(self):
        """
        Return an iterator over BeautifulSoup objects, each a parsed
        tree object of self.urls().
        """
        import urllib2
        from bs4 import BeautifulSoup
        urls = self.urls()
        for url in urls:
            self.url = url
            page = urllib2.urlopen(url)
            soup = BeautifulSoup(page, "html.parser")
            yield soup

    def get_menu(self):
        """
        Retrieve the menu, and store it in self.
        """
        for soup in self.souped():
            if self.find_menu(soup):
                self.clean_menu()
                return
        self.menu = None

    def clean_menu(self):
        self.menu_day = self.menu_day.strip()
        ## flat menus correspond to menus with one empty category
        if self.menu and not type(self.menu[0]) is list:
            self.menu = [self.menu]
        # strip and remove multiple white characters and empty lines
        clean_f = lambda s: " ".join(s.strip().split())
        self.menu = [map(clean_f, [e for e in section if e.strip()]) for section in self.menu]

    def find_menu(self, soup):
        """
        If soup contains a valid menu, store it in self and return True;
        otherwise return False.
        """
        return False


class PrintableMenu(Menu):
    
    LAYOUTS = ["full", "compact", "slack"]

    def __init__(self, on_day):
        super(PrintableMenu, self).__init__(on_day)
        self.set_layout(self.LAYOUTS[0])
        self.include_url = False
        self.include_day = True

    def set_layout(self, layout):
        """Set self.layout to layout."""
        if layout not in self.LAYOUTS:
            raise ValueError(layout + " is not a valid layout")
        else:
            self.layout = layout

    def layout_compact(self):
        """Unicode string of self's menu using compact layout."""
        res = []
        res += [u"*" + self.restaurant + u"*:"]
        if self.include_url and self.url is not None:
            res += [u"(" + self.url + u")"]
        if self.include_day and self.menu_day is not None:
            res += [self.menu_day + u"."]
        if self.menu is not None:
            sections = []
            for k in range(len(self.sections)):
                cur_section = u""
                section_name = self.sections[k][0]
                if section_name:
                    cur_section += self.sections[k] + u": "
                if k < len(self.menu):
                    cur_section += u", ".join(self.menu[k]).replace(u", (", u" (")
                sections += [cur_section]
            res += [u". ".join(sections)]
        return u" ".join(res)

    def layout_full(self):
        """Unicode string of self's menu using full layout."""
        return self.layout_slack()

    def layout_slack(self):
        """Unicode string of self's menu using a layout that works well as Slack message."""
        res = []
        res += [u"*" + self.restaurant + u"*:\n"]
        if self.include_url and self.url is not None:
            res += [u"(" + self.url + u")"]
        ## Slack layout excludes the day
        if self.menu is not None:
            sections = []
            for k in range(len(self.sections)):
                cur_section = u"   - "
                if k:
                    cur_section = u" " + cur_section
                section_name = self.sections[k][0]
                if section_name:
                    cur_section += u"_" + section_name + u"_" + u": "
                if k < len(self.menu):
                    cur_section += u"; ".join(self.menu[k]).replace(u"; (", u" (")
                sections += [cur_section]
            res += [u"\n".join(sections)]
        return u" ".join(res)

    def text(self):
        """Unicode string of self's menu using self.layout."""
        layout_m = getattr(self, "layout_" + self.layout)
        return layout_m()

    def __str__(self):
        return self.text()
        
    def __unicode__(self):
        return str(self)
