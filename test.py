#! /usr/bin/env python2
# -*- coding: utf-8 -*-


restaurants = [u"Mensa"]
localread_restaurants = []
all_restaurants = restaurants + localread_restaurants

ALL = u"ALL"


def main():
    import argparse
    import test.test_restaurants
    import unittest
    parser = argparse.ArgumentParser(
        description="Runs tests for PerPranzo.")
    parser.add_argument("--genoracles", action = "store_true",
                        help="generate .expect oracle files")
    parser.add_argument("rests", nargs="*", default = ALL, 
                        choices = [ALL] + all_restaurants,
                        help="restaurants to be tested (default: %s)" % ALL)
    args = parser.parse_args()
    if type(args.rests) is list:
        restaurants = args.rests
    elif args.rests == ALL:
        restaurants = all_restaurants
    if args.genoracles:
        for r in restaurants:
                print r
                DriverClass = test.test_restaurants.LocalReadDriver \
                              if r in localread_restaurants \
                                 else test.test_restaurants.Driver
                c = DriverClass(r)
                for e in c.process(build_expect = True):
                    pass
    else:
        suite = unittest.TestSuite()
        for r in restaurants:
            suite.addTest(test.test_restaurants.TestRestaurants("test_" + r))
        unittest.TextTestRunner(verbosity=2).run(suite)


if __name__ == "__main__":
    main()
